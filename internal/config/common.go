package config

import (
	"os"

	"github.com/jessevdk/go-flags"
	"github.com/joho/godotenv"
	"github.com/rs/zerolog/log"
)

func LoadConfig() Config {
	var config Config

	_ = godotenv.Load() // get variables from .env file

	var parser = flags.NewParser(&config, flags.Default)
	if _, err := parser.Parse(); err != nil {
		panic(err)
	}

	config.HostName = getHostName()

	return config
}

type Config struct {
	AppEnv string `short:"e" long:"app-env" env:"APP_ENV" description:"Application env name"`

	AppHost     string `short:"h" long:"app-host" env:"APP_HOST" description:"Application server host"`
	AppHttpPort string `long:"app-http-port" env:"APP_HTTP_PORT" description:"Application http server port"`
	AppPort     string `short:"p" long:"app-port" env:"APP_PORT" description:"Application server port"`

	LogPath string `short:"l" long:"log-path" env:"LOG_PATH" description:"Filename for logs"`

	MailServiceHost        string `long:"mail-service-host" env:"MAIL_SERVICE_HOST"`
	MailServicePort        string `long:"mail-service-port" env:"MAIL_SERVICE_PORT"`
	MailServiceHostTimeout int32  `long:"mail-service-timeout" env:"MAIL_SERVICE_TIMEOUT"`

	AuthServiceHost        string `long:"auth-service-host" env:"AUTH_SERVICE_HOST"`
	AuthServicePort        string `long:"auth-service-port" env:"AUTH_SERVICE_PORT"`
	AuthServiceHostTimeout int32  `long:"auth-service-timeout" env:"AUTH_SERVICE_TIMEOUT"`

	AuthzServiceHost        string `long:"authz-service-host" env:"AUTHZ_SERVICE_HOST"`
	AuthzServicePort        string `long:"authz-service-port" env:"AUTHZ_SERVICE_PORT"`
	AuthzServiceHostTimeout int32  `long:"authz-service-timeout" env:"AUTHZ_SERVICE_TIMEOUT"`

	MfaServiceHost        string `long:"mfa-service-host" env:"MFA_SERVICE_HOST"`
	MfaServicePort        string `long:"mfa-service-port" env:"MFA_SERVICE_PORT"`
	MfaServiceHostTimeout int32  `long:"mfa-service-timeout" env:"MFA_SERVICE_TIMEOUT"`

	HostName string
}

func (c *Config) IsStage() bool {
	return c.AppEnv == "stage"
}

func (c *Config) IsProd() bool {
	return c.AppEnv == "production"
}

func (c *Config) UseHeadlessServices() bool {
	return c.IsProd() || c.IsStage()
}

func (c *Config) UseSyslog() bool {
	return c.IsProd() || c.IsStage()
}

func getHostName() string {
	hostName, err := os.Hostname()
	if err != nil {
		log.Warn().Err(err).Msg("can`t get hostname")
	}
	return hostName
}
