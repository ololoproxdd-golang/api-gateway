package transport

import (
	"context"
	"encoding/json"
	kithttp "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"
	"github.com/rs/zerolog/log"
	v1 "gitlab.com/ololoproxdd-golang/api-gateway/pkg/api/v1"
	pkg_auth "gitlab.com/ololoproxdd-golang/auth-service/pkg/api/v1"
	"net/http"
)

func MakeHttpHandler(endpoint *Endpoints, middleware []mux.MiddlewareFunc, authMiddleware []mux.MiddlewareFunc) http.Handler {
	opts := []kithttp.ServerOption{
		kithttp.ServerErrorHandler(NewErrorHandler("http")),
		kithttp.ServerErrorEncoder(encodeError),
		kithttp.ServerBefore(kithttp.PopulateRequestContext),
	}

	pingHandler := kithttp.NewServer(
		endpoint.Ping,
		noDecodeRequest,
		encodeResponse,
		opts...,
	)

	healthHandler := kithttp.NewServer(
		endpoint.Health,
		noDecodeRequest,
		encodeResponse,
		opts...,
	)

	loginHandler := kithttp.NewServer(
		endpoint.Login,
		noDecodeRequest,
		redirectResponse,
		opts...,
	)

	callbackHandler := kithttp.NewServer(
		endpoint.Callback,
		decodeCallbackRequest,
		encodeResponse,
		opts...,
	)

	refreshHandler := kithttp.NewServer(
		endpoint.Refresh,
		decodeRefreshRequest,
		encodeResponse,
		opts...,
	)

	checkOtpHandler := kithttp.NewServer(
		endpoint.CheckOtp,
		decodeCheckOtpRequest,
		encodeResponse,
		opts...,
	)

	getDrugHandler := kithttp.NewServer(
		endpoint.GetDrug,
		noDecodeRequest,
		encodeResponse,
		opts...,
	//append(opts, kithttp.ServerBefore(jwt.HTTPToContext()))...,
	)

	getFoodHandler := kithttp.NewServer(
		endpoint.GetFood,
		noDecodeRequest,
		encodeResponse,
		opts...,
	//append(opts, kithttp.ServerBefore(jwt.HTTPToContext()))...,
	)

	r := mux.NewRouter()
	r.Use(middleware...)

	r.Handle("/api/v1/ping", pingHandler).Methods("GET")
	r.Handle("/api/v1/health", healthHandler).Methods("GET")
	r.Handle("/api/v1/login", loginHandler).Methods("GET")
	r.Handle("/api/v1/callback", callbackHandler)
	r.Handle("/api/v1/refresh", refreshHandler).Methods("POST")
	r.Handle("/api/v1/otp", checkOtpHandler).Methods("POST")

	ar := r.PathPrefix("/ar").Subrouter()
	ar.Use(authMiddleware...)

	ar.Handle("/api/v1/food", getFoodHandler).Methods("GET")
	ar.Handle("/api/v1/drug", getDrugHandler).Methods("GET")

	return r
}

func noDecodeRequest(context.Context, *http.Request) (interface{}, error) {
	return nil, nil
}

func decodeCallbackRequest(_ context.Context, req *http.Request) (request interface{}, err error) {
	callbackReq := pkg_auth.CallbackRequest{
		State: req.FormValue("state"),
		Code:  req.FormValue("code"),
	}

	return callbackReq, nil
}

func decodeRefreshRequest(_ context.Context, req *http.Request) (request interface{}, err error) {
	refreshReq := pkg_auth.RefreshRequest{}
	if err = json.NewDecoder(req.Body).Decode(&refreshReq); err != nil {
		return nil, err
	}
	return refreshReq, nil
}

func decodeCheckOtpRequest(_ context.Context, req *http.Request) (request interface{}, err error) {
	checkRequest := v1.CheckOtpRequest{}
	if err = json.NewDecoder(req.Body).Decode(&checkRequest); err != nil {
		return nil, err
	}
	return checkRequest, nil
}

type errorer interface {
	error() error
}

func encodeResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	if e, ok := response.(errorer); ok && e.error() != nil {
		encodeError(ctx, e.error(), w)
		return nil
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	return json.NewEncoder(w).Encode(response)
}

func redirectResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	if e, ok := response.(errorer); ok && e.error() != nil {
		encodeError(ctx, e.error(), w)
		return nil
	}

	if resp, ok := response.(*pkg_auth.LoginResponse); ok && resp != nil && resp.RedirectUrl != "" {
		http.Redirect(w, &http.Request{}, resp.RedirectUrl, http.StatusTemporaryRedirect)
	}
	return nil
}

func encodeError(_ context.Context, err error, w http.ResponseWriter) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	switch err {
	default:
		w.WriteHeader(http.StatusInternalServerError)
	}
	err = json.NewEncoder(w).Encode(map[string]interface{}{
		"error": err.Error(),
	})
	if err != nil {
		log.Error().Err(err).Msgf("Encode to json failed in encodeError")
	}
}
