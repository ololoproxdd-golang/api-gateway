package transport

import (
	"context"
	"fmt"
	"github.com/go-kit/kit/endpoint"
	"github.com/rs/zerolog/log"
	"gitlab.com/ololoproxdd-golang/api-gateway/internal/service/auth"
	"gitlab.com/ololoproxdd-golang/api-gateway/internal/service/authz"
	"gitlab.com/ololoproxdd-golang/api-gateway/internal/service/health"
	"gitlab.com/ololoproxdd-golang/api-gateway/internal/service/mail"
	"gitlab.com/ololoproxdd-golang/api-gateway/internal/service/mfa"
	"gitlab.com/ololoproxdd-golang/api-gateway/internal/service/resource"
	v1 "gitlab.com/ololoproxdd-golang/api-gateway/pkg/api/v1"
	auth_pkg "gitlab.com/ololoproxdd-golang/auth-service/pkg/api/v1"
	authz_pkg "gitlab.com/ololoproxdd-golang/authz-service/pkg/api/v1"
	mail_pkg "gitlab.com/ololoproxdd-golang/mail-service/pkg/api/v1"
	mfa_pkg "gitlab.com/ololoproxdd-golang/mfa-service/pkg/api/v1"
)

type Endpoints struct {
	Ping     endpoint.Endpoint
	Health   endpoint.Endpoint
	Login    endpoint.Endpoint
	Callback endpoint.Endpoint
	Refresh  endpoint.Endpoint
	CheckOtp endpoint.Endpoint
	GetFood  endpoint.Endpoint
	GetDrug  endpoint.Endpoint
}

func NewEndpoints(
	authS auth.AuthService,
	healthS health.HealthCheck,
	resS resource.ResourceService,
	mfaS mfa.MfaService,
	mailS mail.MailService,
	authzS authz.AuthzService,
) *Endpoints {
	return &Endpoints{
		Login:    makeLoginEndpoint(authS),
		Callback: makeCallbackEndpoint(authS, mfaS, mailS),
		Refresh:  makeRefreshEndpoint(authS),
		Ping:     makePingEndpoint(healthS),
		Health:   makeHealthEndpoint(healthS),
		GetFood:  makeFoodEndpoint(resS),
		GetDrug:  makeDrugEndpoint(resS),
		CheckOtp: makeOtpEndpoint(mfaS, authzS, authS),
	}
}

func makePingEndpoint(c health.HealthCheck) endpoint.Endpoint {
	return func(context.Context, interface{}) (interface{}, error) {
		return c.Ping()
	}
}

func makeHealthEndpoint(c health.HealthCheck) endpoint.Endpoint {
	return func(context.Context, interface{}) (interface{}, error) {
		return c.Health()
	}
}

func makeLoginEndpoint(s auth.AuthService) endpoint.Endpoint {
	return func(ctx context.Context, req interface{}) (interface{}, error) {
		return s.Login()
	}
}

func makeCallbackEndpoint(authS auth.AuthService, mfaS mfa.MfaService, mailS mail.MailService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		//callback to auth service to finish OAuth2
		req, ok := request.(auth_pkg.CallbackRequest)
		if !ok {
			log.Fatal().
				Interface("request", request).
				Msg("makeCallbackEndpoint not type of auth_pkg.CallbackRequest")
		}

		callbackResp, err := authS.Callback(req)
		if err != nil {
			return nil, err
		}
		fmt.Println(callbackResp)

		//create otp in mfa service
		mfaReq := mfa_pkg.CreateRequest{Email: callbackResp.Email, Type: mfa_pkg.MfaType_HOTP}
		mfaResp, err := mfaS.Create(mfaReq)
		if err != nil {
			return nil, err
		}

		//send otp via email service
		mailReq := mail_pkg.SendRequest{
			To:          callbackResp.Email,
			Subject:     "OTP password",
			ContentType: "text",
			Body:        mfaResp.Value,
		}
		mailResp, err := mailS.Send(mailReq)
		if err != nil {
			return nil, err
		}

		fmt.Println(mailResp)
		return callbackResp, nil
	}
}

func makeRefreshEndpoint(s auth.AuthService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req, ok := request.(auth_pkg.RefreshRequest)
		if !ok {
			log.Fatal().
				Interface("request", request).
				Msg("makeRefreshEndpoint not type of auth_pkg.RefreshRequest")
		}

		return s.Refresh(req)
	}
}

func makeOtpEndpoint(mfaS mfa.MfaService, authzS authz.AuthzService, authS auth.AuthService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req, ok := request.(v1.CheckOtpRequest)
		if !ok {
			log.Fatal().
				Interface("request", request).
				Msg("makeOtpEndpoint not type of v1.CheckOtpRequest")
		}

		mfaReq := mfa_pkg.CheckRequest{Value: req.Value, Email: req.Email, Type: mfa_pkg.MfaType_HOTP}
		_, err := mfaS.Validate(mfaReq)
		if err != nil {
			return nil, err
		}

		authzReq := authz_pkg.CreateJwtRequest{
			Email:        req.Email,
			AccessToken:  req.AccessToken,
			RefreshToken: req.RefreshToken,
			Company:      "",
		}
		authzResp, err := authzS.CreateJwt(authzReq)
		if err != nil {
			return nil, err
		}

		fmt.Println(authzResp)
		return authzResp, nil
	}
}

func makeDrugEndpoint(s resource.ResourceService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		return s.GetDrug()
	}
}

func makeFoodEndpoint(s resource.ResourceService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		return s.GetFood()
	}
}
