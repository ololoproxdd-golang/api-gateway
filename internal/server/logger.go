package server

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"time"
)

// init logger
func (s *server) initLogger() {
	zerolog.MessageFieldName = "MESSAGE"
	zerolog.LevelFieldName = "LEVEL"

	zerolog.SetGlobalLevel(s.getLoggerLevel())
	log.Logger = log.Output(s.getLoggerWriter()).
		With().Str("PROGRAM", "api-gateway").
		Str("HOST_NAME", s.config.HostName).
		Logger()
}

func (s *server) getLoggerWriter() io.Writer {
	var writer io.Writer
	writer = os.Stdout

	if s.config.UseSyslog() {
		var err error
		writer, err = os.OpenFile(s.config.LogPath, os.O_WRONLY|os.O_APPEND, os.FileMode(0666))
		if err != nil {
			log.Warn().Err(err).Msg("cannot open logs file")
			writer = os.Stdout
		} else if !s.config.IsProd() {
			writer = io.MultiWriter(writer, os.Stdout)
		}
	}
	return writer
}

func (s *server) getLoggerLevel() zerolog.Level {
	if s.config.IsProd() {
		return zerolog.InfoLevel
	}

	return zerolog.DebugLevel
}

type statusWriter struct {
	http.ResponseWriter
	status int
	length int
}

func (w *statusWriter) WriteHeader(status int) {
	w.status = status
	w.ResponseWriter.WriteHeader(status)
}

func (w *statusWriter) Write(b []byte) (int, error) {
	if w.status == 0 {
		w.status = 200
	}
	n, err := w.ResponseWriter.Write(b)
	w.length += n
	return n, err
}

func (s *server) loggerHTTP(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		b := bytes.NewBuffer(make([]byte, 0))
		var body interface{}
		err := json.NewDecoder(io.TeeReader(req.Body, b)).Decode(&body)
		switch {
		case err == io.EOF:
			break
		case err != nil:
			log.Error().Err(err).Msg("decode body")
		}
		defer req.Body.Close()
		req.Body = ioutil.NopCloser(b)

		start := time.Now()
		sw := &statusWriter{ResponseWriter: w}
		next.ServeHTTP(sw, req)
		duration := time.Since(start)

		log.Info().
			Str("Method", req.Method+": "+req.URL.Path).
			Dur("Duration", duration).
			Msg(fmt.Sprintf("Duration:%s\tStatus:%d", duration, sw.status))
	})
}
