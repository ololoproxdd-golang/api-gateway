package server

import (
	"github.com/pkg/errors"
	v1 "gitlab.com/ololoproxdd-golang/authz-service/pkg/api/v1"
	"net/http"
	"strings"
)

var (
	UnauthorizedErr = errors.New("Unauthorized error")
)

func (s *server) authHTTP(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		token := req.Header.Get("Authorization")
		splitToken := strings.Split(token, "Bearer")
		if len(splitToken) != 2 {
			w.WriteHeader(http.StatusUnauthorized)
			return
		}

		token = strings.TrimSpace(splitToken[1])
		if token == "" {
			w.WriteHeader(http.StatusUnauthorized)
			return
		}

		err := s.authorization(token)
		if err != nil {
			w.WriteHeader(http.StatusUnauthorized)
			return
		}

		next.ServeHTTP(w, req)
	})
}

func (s *server) authorization(token string) error {
	//check in authz service
	req := v1.ValidateJwtRequest{
		JwtToken: token,
	}

	resp, err := s.getAuthzClient().ValidateJwt(req)
	if err != nil || !resp.IsValid {
		return UnauthorizedErr
	}

	return nil
}
