package server

import (
	"github.com/rs/zerolog/log"
	"sync"
)

func (s *server) runHttpServer(wait *sync.WaitGroup) {
	defer wait.Done()
	srv := s.getTransportHttp()
	log.Info().Msgf("running http server address: %s", srv.Addr)
	err := s.httpServer.ListenAndServe()
	log.Info().Err(err).Msg("closed http server")
}
