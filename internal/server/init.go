package server

import (
	"fmt"
	"github.com/rs/zerolog/log"
	"gitlab.com/ololoproxdd-golang/api-gateway/internal/proxy"
	"gitlab.com/ololoproxdd-golang/api-gateway/internal/service/auth"
	"gitlab.com/ololoproxdd-golang/api-gateway/internal/service/authz"
	"gitlab.com/ololoproxdd-golang/api-gateway/internal/service/health"
	"gitlab.com/ololoproxdd-golang/api-gateway/internal/service/mail"
	"gitlab.com/ololoproxdd-golang/api-gateway/internal/service/mfa"
	"gitlab.com/ololoproxdd-golang/api-gateway/internal/service/resource"
	"gitlab.com/ololoproxdd-golang/api-gateway/internal/transport"
	"google.golang.org/grpc"
	"google.golang.org/grpc/balancer/roundrobin"
	golog "log"
	"net/http"
	"os"
	"time"
)

func (s *server) getEndpoints() *transport.Endpoints {
	if s.endpoints != nil {
		return s.endpoints
	}

	s.endpoints = transport.NewEndpoints(
		s.getAuthClient(),
		s.getHealthService(),
		s.getResourceService(),
		s.getMfaClient(),
		s.getMailClient(),
		s.getAuthzClient(),
	)
	log.Info().Msg("created server endpoints")

	return s.endpoints
}

func (s *server) getTransportHttp() *http.Server {
	if s.httpServer != nil {
		return s.httpServer
	}
	middleware := s.newMiddleware()
	authMiddleware := s.newAuthMiddleware()

	s.httpServer = &http.Server{
		Handler:      transport.MakeHttpHandler(s.getEndpoints(), middleware.http, authMiddleware.http),
		Addr:         fmt.Sprintf("%s:%s", s.config.AppHost, s.config.AppHttpPort),
		ErrorLog:     golog.New(os.Stdout, "http: ", golog.LstdFlags),
		WriteTimeout: 2 * time.Second,
		ReadTimeout:  2 * time.Second,
	}

	log.Info().Msgf("create transport - http")

	return s.httpServer
}

func (s *server) getMailConn() *grpc.ClientConn {
	if s.mailConn != nil {
		return s.mailConn
	}

	var conn *grpc.ClientConn
	var err error
	var connString string

	if s.config.UseHeadlessServices() {
		connString = fmt.Sprintf("dns:///%s:%s", s.config.MailServiceHost, s.config.MailServicePort)
		conn, err = grpc.Dial(connString, grpc.WithInsecure(), grpc.WithBalancerName(roundrobin.Name))
	} else {
		connString = fmt.Sprintf("%s:%s", s.config.MailServiceHost, s.config.MailServicePort)
		conn, err = grpc.Dial(connString, grpc.WithInsecure())
	}

	if err != nil {
		log.Fatal().Err(err).Msgf("mail client connect: %s", connString)
		return nil
	}

	s.mailConn = conn
	log.Info().Msgf("created mail client connection: %s ...", connString)

	return s.mailConn
}

func (s *server) getMailEndpoints() *proxy.MailEndpoints {
	if s.mailEndpoints != nil {
		return s.mailEndpoints
	}

	s.mailEndpoints = proxy.NewMailEndpoints(s.getMailConn())
	log.Info().Msg("created mail endpoints ... ")

	return s.mailEndpoints
}

func (s *server) getMailClient() mail.MailService {
	if s.mailClient != nil {
		return s.mailClient
	}

	s.mailClient = mail.NewMailService(s.getMailEndpoints())
	log.Info().Msg("created mail client ... ")

	return s.mailClient
}

func (s *server) getAuthConn() *grpc.ClientConn {
	if s.authConn != nil {
		return s.authConn
	}

	var conn *grpc.ClientConn
	var err error
	var connString string

	connString = fmt.Sprintf("%s:%s", s.config.AuthServiceHost, s.config.AuthServicePort)
	conn, err = grpc.Dial(connString, grpc.WithInsecure())

	if err != nil {
		log.Fatal().Err(err).Msgf("auth client connect: %s", connString)
		return nil
	}

	s.authConn = conn
	log.Info().Msgf("created auth client connection: %s", connString)

	return s.authConn
}

func (s *server) getAuthEndpoints() *proxy.AuthEndpoints {
	if s.authEndpoints != nil {
		return s.authEndpoints
	}

	s.authEndpoints = proxy.NewAuthEndpoints(s.getAuthConn())
	log.Info().Msg("created auth endpoints ... ")

	return s.authEndpoints
}

func (s *server) getAuthClient() auth.AuthService {
	if s.authClient != nil {
		return s.authClient
	}

	s.authClient = auth.NewAuthService(s.getAuthEndpoints())
	log.Info().Msg("created auth client ... ")

	return s.authClient
}

func (s *server) getMfaConn() *grpc.ClientConn {
	if s.mfaConn != nil {
		return s.mfaConn
	}

	var conn *grpc.ClientConn
	var err error
	var connString string

	connString = fmt.Sprintf("%s:%s", s.config.MfaServiceHost, s.config.MfaServicePort)
	conn, err = grpc.Dial(connString, grpc.WithInsecure())

	if err != nil {
		log.Fatal().Err(err).Msgf("mfa client connect: %s", connString)
		return nil
	}

	s.mfaConn = conn
	log.Info().Msgf("created mfa client connection: %s", connString)

	return s.mfaConn
}

func (s *server) getMfaEndpoints() *proxy.MfaEndpoints {
	if s.mfaEndpoints != nil {
		return s.mfaEndpoints
	}

	s.mfaEndpoints = proxy.NewMfaEndpoints(s.getMfaConn())
	log.Info().Msg("created mfa endpoints ... ")

	return s.mfaEndpoints
}

func (s *server) getMfaClient() mfa.MfaService {
	if s.mfaClient != nil {
		return s.mfaClient
	}

	s.mfaClient = mfa.NewMfaService(s.getMfaEndpoints())
	log.Info().Msg("created mfa client ... ")

	return s.mfaClient
}

func (s *server) getAuthzConn() *grpc.ClientConn {
	if s.authzConn != nil {
		return s.authzConn
	}

	var conn *grpc.ClientConn
	var err error
	var connString string

	connString = fmt.Sprintf("%s:%s", s.config.AuthzServiceHost, s.config.AuthzServicePort)
	conn, err = grpc.Dial(connString, grpc.WithInsecure())

	if err != nil {
		log.Fatal().Err(err).Msgf("authz client connect: %s", connString)
		return nil
	}

	s.authzConn = conn
	log.Info().Msgf("created authz client connection: %s", connString)

	return s.authzConn
}

func (s *server) getAuthzEndpoints() *proxy.AuthzEndpoints {
	if s.authzEndpoints != nil {
		return s.authzEndpoints
	}

	s.authzEndpoints = proxy.NewAuthzEndpoints(s.getAuthzConn())
	log.Info().Msg("created authz endpoints ... ")

	return s.authzEndpoints
}

func (s *server) getAuthzClient() authz.AuthzService {
	if s.authzClient != nil {
		return s.authzClient
	}

	s.authzClient = authz.NewAuthzService(s.getAuthzEndpoints())
	log.Info().Msg("created authz client ... ")

	return s.authzClient
}

func (s *server) getHealthService() health.HealthCheck {
	if s.healthService != nil {
		return s.healthService
	}
	s.healthService = health.NewHealthService(s.getAuthEndpoints(), s.getAuthzEndpoints(), s.getMailEndpoints(), s.getMfaEndpoints())
	log.Info().Msgf("created health service")

	return s.healthService
}

func (s *server) getResourceService() resource.ResourceService {
	if s.resourceService != nil {
		return s.resourceService
	}

	s.resourceService = resource.NewResourceService()
	log.Info().Msg("created resource service")

	return s.resourceService
}
