package server

import (
	"context"
	"github.com/rs/zerolog/log"
	"gitlab.com/ololoproxdd-golang/api-gateway/internal/config"
	"gitlab.com/ololoproxdd-golang/api-gateway/internal/proxy"
	"gitlab.com/ololoproxdd-golang/api-gateway/internal/service/auth"
	"gitlab.com/ololoproxdd-golang/api-gateway/internal/service/authz"
	"gitlab.com/ololoproxdd-golang/api-gateway/internal/service/health"
	"gitlab.com/ololoproxdd-golang/api-gateway/internal/service/mail"
	"gitlab.com/ololoproxdd-golang/api-gateway/internal/service/mfa"
	"gitlab.com/ololoproxdd-golang/api-gateway/internal/service/resource"
	"gitlab.com/ololoproxdd-golang/api-gateway/internal/transport"
	"google.golang.org/grpc"
	"net/http"
	"sync"
	"time"
)

type server struct {
	httpServer *http.Server

	config    config.Config
	endpoints *transport.Endpoints

	healthService   health.HealthCheck
	resourceService resource.ResourceService

	mailEndpoints  *proxy.MailEndpoints
	authEndpoints  *proxy.AuthEndpoints
	authzEndpoints *proxy.AuthzEndpoints
	mfaEndpoints   *proxy.MfaEndpoints

	mailConn  *grpc.ClientConn
	authConn  *grpc.ClientConn
	authzConn *grpc.ClientConn
	mfaConn   *grpc.ClientConn

	mailClient  mail.MailService
	authClient  auth.AuthService
	authzClient authz.AuthzService
	mfaClient   mfa.MfaService
}

func NewServer(config config.Config) *server {
	return &server{config: config}
}

// close all descriptors
func (s *server) Close() {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := s.httpServer.Shutdown(ctx); err != nil {
		log.Warn().Err(err).Msg("closing http server")
	}
}

// open init structure
func (s *server) Open() error {
	s.initLogger()
	s.registerOsSignal()

	return nil
}

func (s *server) Run() {
	wg := sync.WaitGroup{}

	wg.Add(1)
	go s.runHttpServer(&wg)

	wg.Wait()
	log.Info().Msg("api-gateway stopped...")
}
