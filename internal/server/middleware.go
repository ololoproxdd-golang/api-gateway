package server

import (
	"github.com/gorilla/mux"
	"net/http"
)

type middleware struct {
	http []mux.MiddlewareFunc
}

func (s *server) newMiddleware() middleware {
	return middleware{
		http: []mux.MiddlewareFunc{
			s.loggerHTTP,
			s.recoveryHTTP,
		},
	}
}

func (s *server) newAuthMiddleware() middleware {
	return middleware{
		http: []mux.MiddlewareFunc{
			s.authHTTP,
		},
	}
}

func (s *server) recoveryHTTP(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		defer func() {
			if err := recover(); err != nil {
				w.WriteHeader(http.StatusInternalServerError)
			}
		}()
		next.ServeHTTP(w, req)
	})
}
