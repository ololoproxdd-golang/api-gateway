package proxy

import (
	"context"
	"github.com/go-kit/kit/endpoint"
	"github.com/golang/protobuf/ptypes/empty"
	"github.com/pkg/errors"
	"github.com/rs/zerolog/log"
	mail "gitlab.com/ololoproxdd-golang/mail-service/pkg/api/v1"
	"google.golang.org/grpc"
)

type MailEndpoints struct {
	Ping endpoint.Endpoint
	Send endpoint.Endpoint
}

func NewMailEndpoints(grpcClient *grpc.ClientConn) *MailEndpoints {
	client := mail.NewMailServiceClient(grpcClient)
	endpoints := &MailEndpoints{
		Ping: makeMailPingEndpoint(client),
		Send: makeMailSendEndpoint(client),
	}
	return endpoints
}

func makeMailPingEndpoint(s mail.MailServiceClient) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		res, err := s.Ping(ctx, &empty.Empty{})
		if err != nil {
			return nil, err
		}

		return res, nil
	}
}

func makeMailSendEndpoint(s mail.MailServiceClient) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req, ok := request.(mail.SendRequest)
		if !ok {
			log.Fatal().
				Interface("request", request).
				Msg("makeMailSendEndpoint not type of mail.SendRequest")
			return nil, errors.New("")
		}
		res, err := s.Send(ctx, &req)
		if err != nil {
			return nil, errors.Wrapf(err, "mail service error")
		}

		return res, nil
	}
}
