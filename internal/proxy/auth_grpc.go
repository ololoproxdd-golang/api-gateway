package proxy

import (
	"context"
	"github.com/go-kit/kit/endpoint"
	"github.com/golang/protobuf/ptypes/empty"
	"github.com/pkg/errors"
	"github.com/rs/zerolog/log"
	auth "gitlab.com/ololoproxdd-golang/auth-service/pkg/api/v1"
	"google.golang.org/grpc"
)

type AuthEndpoints struct {
	Ping     endpoint.Endpoint
	Login    endpoint.Endpoint
	Callback endpoint.Endpoint
	Refresh  endpoint.Endpoint
}

func NewAuthEndpoints(grpcClient *grpc.ClientConn) *AuthEndpoints {
	client := auth.NewAuthServiceClient(grpcClient)
	endpoints := &AuthEndpoints{
		Ping:     makeAuthPingEndpoint(client),
		Login:    makeAuthLoginEndpoint(client),
		Callback: makeCallbackEndpoint(client),
		Refresh:  makeRefreshEndpoint(client),
	}

	return endpoints
}

func makeAuthPingEndpoint(s auth.AuthServiceClient) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		res, err := s.Ping(ctx, &empty.Empty{})
		if err != nil {
			return nil, err
		}

		return res, nil
	}
}

func makeAuthLoginEndpoint(s auth.AuthServiceClient) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		res, err := s.Login(ctx, &empty.Empty{})
		if err != nil {
			return nil, errors.Wrapf(err, "auth service error")
		}

		return res, nil
	}
}

func makeCallbackEndpoint(s auth.AuthServiceClient) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req, ok := request.(*auth.CallbackRequest)
		if !ok {
			log.Fatal().
				Interface("request", request).
				Msg("makeCallbackEndpoint not type of *auth.CallbackRequest")
			return nil, errors.New("")
		}

		res, err := s.Callback(ctx, req)
		if err != nil {
			return nil, errors.Wrapf(err, "auth service error")
		}

		return res, nil
	}
}

func makeRefreshEndpoint(s auth.AuthServiceClient) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req, ok := request.(*auth.RefreshRequest)
		if !ok {
			log.Fatal().
				Interface("request", request).
				Msg("makeCallbackEndpoint not type of auth.RefreshRequest")
			return nil, errors.New("")
		}

		res, err := s.Refresh(ctx, req)
		if err != nil {
			return nil, errors.Wrapf(err, "auth service error")
		}

		return res, nil
	}
}
