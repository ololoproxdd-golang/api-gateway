package proxy

import (
	"context"
	"github.com/go-kit/kit/endpoint"
	"github.com/golang/protobuf/ptypes/empty"
	"github.com/pkg/errors"
	"github.com/rs/zerolog/log"
	authz "gitlab.com/ololoproxdd-golang/authz-service/pkg/api/v1"
	"google.golang.org/grpc"
)

type AuthzEndpoints struct {
	Ping        endpoint.Endpoint
	CreateJwt   endpoint.Endpoint
	ValidateJwt endpoint.Endpoint
}

func NewAuthzEndpoints(grpcClient *grpc.ClientConn) *AuthzEndpoints {
	client := authz.NewAuthzServiceClient(grpcClient)
	endpoints := &AuthzEndpoints{
		Ping:        makeAuthzPingEndpoint(client),
		CreateJwt:   makeAuthzCreateJwtEndpoint(client),
		ValidateJwt: makeAuthzValidateJwtEndpoint(client),
	}

	return endpoints
}

func makeAuthzPingEndpoint(s authz.AuthzServiceClient) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		res, err := s.Ping(ctx, &empty.Empty{})
		if err != nil {
			return nil, err
		}

		return res, nil
	}
}

func makeAuthzCreateJwtEndpoint(s authz.AuthzServiceClient) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req, ok := request.(authz.CreateJwtRequest)
		if !ok {
			log.Fatal().
				Interface("request", request).
				Msg("makeAuthzCreateEndpoint not type of authz.CreateJwtRequest")
			return nil, errors.New("")
		}
		res, err := s.CreateJwt(ctx, &req)
		if err != nil {
			return nil, errors.Wrapf(err, "authz service error")
		}

		return res, nil
	}
}

func makeAuthzValidateJwtEndpoint(s authz.AuthzServiceClient) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req, ok := request.(authz.ValidateJwtRequest)
		if !ok {
			log.Fatal().
				Interface("request", request).
				Msg("makeAuthzValidateEndpoint not type of authz.ValidateJwtRequest")
			return nil, errors.New("")
		}
		res, err := s.ValidateJwt(ctx, &req)
		if err != nil {
			return nil, errors.Wrapf(err, "authz service error")
		}

		return res, nil
	}
}
