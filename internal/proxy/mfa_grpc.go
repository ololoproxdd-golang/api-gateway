package proxy

import (
	"context"
	"github.com/go-kit/kit/endpoint"
	"github.com/golang/protobuf/ptypes/empty"
	"github.com/pkg/errors"
	"github.com/rs/zerolog/log"
	mfa "gitlab.com/ololoproxdd-golang/mfa-service/pkg/api/v1"
	"google.golang.org/grpc"
)

type MfaEndpoints struct {
	Ping     endpoint.Endpoint
	Create   endpoint.Endpoint
	Validate endpoint.Endpoint
}

func NewMfaEndpoints(grpcClient *grpc.ClientConn) *MfaEndpoints {
	client := mfa.NewMfaServiceClient(grpcClient)
	endpoints := &MfaEndpoints{
		Ping:     makeMfaPingEndpoint(client),
		Create:   makeMfaCreateEndpoint(client),
		Validate: makeMfaCheckEndpoint(client),
	}

	return endpoints
}

func makeMfaPingEndpoint(s mfa.MfaServiceClient) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		res, err := s.Ping(ctx, &empty.Empty{})
		if err != nil {
			return nil, err
		}

		return res, nil
	}
}

func makeMfaCreateEndpoint(s mfa.MfaServiceClient) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req, ok := request.(mfa.CreateRequest)
		if !ok {
			log.Fatal().
				Interface("request", request).
				Msg("makeMfaCreateEndpoint not type of mfa.CreateRequest")
			return nil, errors.New("")
		}
		res, err := s.Create(ctx, &req)
		if err != nil {
			return nil, errors.Wrapf(err, "mfa service error")
		}

		return res, nil
	}
}

func makeMfaCheckEndpoint(s mfa.MfaServiceClient) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req, ok := request.(mfa.CheckRequest)
		if !ok {
			log.Fatal().
				Interface("request", request).
				Msg("makeMfaCheckEndpoint not type of mfa.CheckRequest")
			return nil, errors.New("")
		}
		res, err := s.Check(ctx, &req)
		if err != nil {
			return nil, errors.Wrapf(err, "mfa service error")
		}

		return res, nil
	}
}
