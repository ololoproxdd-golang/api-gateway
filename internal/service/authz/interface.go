package authz

import "gitlab.com/ololoproxdd-golang/authz-service/pkg/api/v1"

type AuthzService interface {
	CreateJwt(v1.CreateJwtRequest) (*v1.CreateJwtResponse, error)
	ValidateJwt(v1.ValidateJwtRequest) (*v1.ValidateJwtResponse, error)
}
