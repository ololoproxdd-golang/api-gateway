package authz

import (
	"context"
	"github.com/go-kit/kit/endpoint"
	"github.com/rs/zerolog/log"
	"gitlab.com/ololoproxdd-golang/api-gateway/internal/proxy"
	"gitlab.com/ololoproxdd-golang/authz-service/pkg/api/v1"
)

type authzService struct {
	createJwt   endpoint.Endpoint
	validateJwt endpoint.Endpoint
}

func NewAuthzService(endpoints *proxy.AuthzEndpoints) AuthzService {
	return &authzService{
		createJwt:   endpoints.CreateJwt,
		validateJwt: endpoints.ValidateJwt,
	}
}

func (s *authzService) CreateJwt(req v1.CreateJwtRequest) (*v1.CreateJwtResponse, error) {
	res, err := s.createJwt(context.Background(), req)
	if err != nil {
		return nil, err
	}

	resp, ok := res.(*v1.CreateJwtResponse)
	if !ok {
		log.Fatal().Msg("response from transport is not *v1.CreateJwtResponse type")
	}
	return resp, nil
}

func (s *authzService) ValidateJwt(req v1.ValidateJwtRequest) (*v1.ValidateJwtResponse, error) {
	res, err := s.validateJwt(context.Background(), req)
	if err != nil {
		return nil, err
	}

	resp, ok := res.(*v1.ValidateJwtResponse)
	if !ok {
		log.Fatal().Msg("response from transport is not *v1.ValidateJwtResponse type")
	}
	return resp, nil
}
