package auth

import (
	v1 "gitlab.com/ololoproxdd-golang/auth-service/pkg/api/v1"
)

type AuthService interface {
	Login() (*v1.LoginResponse, error)
	Callback(v1.CallbackRequest) (*v1.AuthResponse, error)
	Refresh(v1.RefreshRequest) (*v1.AuthResponse, error)
}
