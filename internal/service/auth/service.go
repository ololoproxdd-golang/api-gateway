package auth

import (
	"context"
	"github.com/go-kit/kit/endpoint"
	"github.com/golang/protobuf/ptypes/empty"
	"github.com/rs/zerolog/log"
	"gitlab.com/ololoproxdd-golang/api-gateway/internal/proxy"
	"gitlab.com/ololoproxdd-golang/auth-service/pkg/api/v1"
)

type authService struct {
	login    endpoint.Endpoint
	callback endpoint.Endpoint
	refresh  endpoint.Endpoint
}

func NewAuthService(endpoints *proxy.AuthEndpoints) AuthService {
	return &authService{
		login:    endpoints.Login,
		callback: endpoints.Callback,
		refresh:  endpoints.Refresh,
	}
}

func (s *authService) Login() (*v1.LoginResponse, error) {
	res, err := s.login(context.Background(), empty.Empty{})
	if err != nil {
		return nil, err
	}

	resp, ok := res.(*v1.LoginResponse)
	if !ok {
		log.Fatal().Msg("response from transport is not *v1.LoginResponse type")
	}
	return resp, nil
}

func (s *authService) Callback(req v1.CallbackRequest) (*v1.AuthResponse, error) {
	res, err := s.callback(context.Background(), &req)
	if err != nil {
		return nil, err
	}

	resp, ok := res.(*v1.AuthResponse)
	if !ok {
		log.Fatal().Msg("response from transport is not *v1.AuthResponse type")
	}
	return resp, nil
}

func (s *authService) Refresh(req v1.RefreshRequest) (*v1.AuthResponse, error) {
	res, err := s.refresh(context.Background(), &req)
	if err != nil {
		return nil, err
	}

	resp, ok := res.(*v1.AuthResponse)
	if !ok {
		log.Fatal().Msg("response from transport is not *v1.AuthResponse type")
	}
	return resp, nil
}
