package mfa

import (
	"context"
	"github.com/go-kit/kit/endpoint"
	"github.com/pkg/errors"
	"github.com/rs/zerolog/log"
	"gitlab.com/ololoproxdd-golang/api-gateway/internal/proxy"
	"gitlab.com/ololoproxdd-golang/mfa-service/pkg/api/v1"
)

type mfaService struct {
	create   endpoint.Endpoint
	validate endpoint.Endpoint
}

func NewMfaService(endpoints *proxy.MfaEndpoints) MfaService {
	return &mfaService{
		create:   endpoints.Create,
		validate: endpoints.Validate,
	}
}

func (s *mfaService) Create(req v1.CreateRequest) (*v1.CreateResponse, error) {
	res, err := s.create(context.Background(), req)
	if err != nil {
		return nil, errors.Wrapf(err, "create OPT from mfa service failed")
	}

	resp, ok := res.(*v1.CreateResponse)
	if !ok {
		log.Fatal().Msg("response from transport is not *v1.CreateResponse type")
	}

	return resp, nil
}

func (s *mfaService) Validate(req v1.CheckRequest) (*v1.CheckResponse, error) {
	res, err := s.validate(context.Background(), req)
	if err != nil {
		return nil, err
	}

	resp, ok := res.(*v1.CheckResponse)
	if !ok {
		log.Fatal().Msg("response from transport is not *v1.CheckResponse type")
	}

	return resp, nil
}
