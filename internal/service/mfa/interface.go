package mfa

import (
	"gitlab.com/ololoproxdd-golang/mfa-service/pkg/api/v1"
)

type MfaService interface {
	Create(v1.CreateRequest) (*v1.CreateResponse, error)
	Validate(v1.CheckRequest) (*v1.CheckResponse, error)
}
