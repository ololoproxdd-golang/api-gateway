package resource

import (
	"encoding/json"
	"net/http"
)

type resourceService struct {
}

func NewResourceService() ResourceService {
	return &resourceService{}
}

func (s *resourceService) getRequest(url string) (interface{}, error) {
	resp, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	//bodyBytes, err := ioutil.ReadAll(resp.Body)
	//if err != nil {
	//	return "", err
	//}
	//bodyString := string(bodyBytes)
	//
	//return bodyString, nil

	var target interface{}
	err = json.NewDecoder(resp.Body).Decode(&target)
	if err != nil {
		return nil, err
	}

	return target, nil
}

func (s *resourceService) GetFood() (interface{}, error) {
	return s.getRequest("https://api.fda.gov/food/enforcement.json?search=report_date:[20040101+TO+20131231]&limit=1")
}

func (s *resourceService) GetDrug() (interface{}, error) {
	return s.getRequest("https://api.fda.gov/drug/event.json?limit=1")
}
