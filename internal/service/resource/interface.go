package resource

type ResourceService interface {
	GetFood() (interface{}, error)
	GetDrug() (interface{}, error)
}
