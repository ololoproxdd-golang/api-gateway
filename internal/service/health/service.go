package health

import (
	"context"
	"github.com/go-kit/kit/endpoint"
	"github.com/golang/protobuf/ptypes/empty"
	"github.com/rs/zerolog/log"
	"gitlab.com/ololoproxdd-golang/api-gateway/internal/proxy"
	"gitlab.com/ololoproxdd-golang/api-gateway/pkg/api/v1"
	"sync"
)

type health struct {
	authPing  endpoint.Endpoint
	authzPing endpoint.Endpoint
	mailPing  endpoint.Endpoint
	mfaPing   endpoint.Endpoint
}

func NewHealthService(
	authE *proxy.AuthEndpoints,
	authzE *proxy.AuthzEndpoints,
	mailE *proxy.MailEndpoints,
	mfaE *proxy.MfaEndpoints,
) HealthCheck {
	return &health{
		authPing:  authE.Ping,
		authzPing: authzE.Ping,
		mailPing:  mailE.Ping,
		mfaPing:   mfaE.Ping,
	}
}

func (h health) Ping() (v1.PingResponse, error) {
	return v1.PingResponse{Message: "Pong"}, nil
}

func (h health) Health() (v1.HealthResponse, error) {
	wg := sync.WaitGroup{}
	wg.Add(1)
	go func() {
		defer wg.Done()
		_, err := h.authPing(context.Background(), empty.Empty{})
		if err != nil {
			log.Fatal().Err(err).Msg("Auth service health check error")
		}
	}()

	wg.Add(1)
	go func() {
		defer wg.Done()
		_, err := h.authzPing(context.Background(), empty.Empty{})
		if err != nil {
			log.Fatal().Err(err).Msg("Authz service health check error")
		}
	}()

	wg.Add(1)
	go func() {
		defer wg.Done()
		_, err := h.mailPing(context.Background(), empty.Empty{})
		if err != nil {
			log.Fatal().Err(err).Msg("Mail service health check error")
		}
	}()

	wg.Add(1)
	go func() {
		defer wg.Done()
		_, err := h.mfaPing(context.Background(), empty.Empty{})
		if err != nil {
			log.Fatal().Err(err).Msg("MFA service health check error")
		}
	}()

	wg.Wait()

	return v1.HealthResponse{Message: "Status: Ok"}, nil
}
