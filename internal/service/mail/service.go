package mail

import (
	"context"
	"github.com/go-kit/kit/endpoint"
	"github.com/pkg/errors"
	"github.com/rs/zerolog/log"
	"gitlab.com/ololoproxdd-golang/api-gateway/internal/proxy"
	"gitlab.com/ololoproxdd-golang/mail-service/pkg/api/v1"
)

type mailService struct {
	send endpoint.Endpoint
}

func NewMailService(endpoints *proxy.MailEndpoints) MailService {
	return &mailService{send: endpoints.Send}
}

func (s *mailService) Send(req v1.SendRequest) (*v1.SendResponse, error) {
	res, err := s.send(context.Background(), req)
	if err != nil {
		return nil, errors.Wrapf(err, "send email from mail service failed")
	}

	resp, ok := res.(*v1.SendResponse)
	if !ok {
		log.Fatal().Msg("response from transport is not *v1.SendResponse type")
	}

	return resp, nil
}
