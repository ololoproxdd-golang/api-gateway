package mail

import (
	"gitlab.com/ololoproxdd-golang/mail-service/pkg/api/v1"
)

type MailService interface {
	Send(v1.SendRequest) (*v1.SendResponse, error)
}
