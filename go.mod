module gitlab.com/ololoproxdd-golang/api-gateway

go 1.13

require (
	github.com/go-kit/kit v0.9.0
	github.com/golang/protobuf v1.3.2
	github.com/gorilla/mux v1.7.3
	github.com/jessevdk/go-flags v1.4.0
	github.com/joho/godotenv v1.3.0
	github.com/pkg/errors v0.8.1
	github.com/rs/zerolog v1.17.2
	gitlab.com/ololoproxdd-golang/auth-service v1.0.3
	gitlab.com/ololoproxdd-golang/authz-service v1.0.1
	gitlab.com/ololoproxdd-golang/mail-service v1.0.1
	gitlab.com/ololoproxdd-golang/mfa-service v1.0.1
	golang.org/x/oauth2 v0.0.0-20191202225959-858c2ad4c8b6 // indirect
	google.golang.org/grpc v1.25.1
)
